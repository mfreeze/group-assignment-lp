#!/usr/bin/env python3

import sys
from collections import Counter

if len(sys.argv) != 4:
    print("Script takes exactly three argument: csv_file, "
          "nb_groups, max_size_of_groups.", file=sys.stderr)
    sys.exit(1)

csv_file = sys.argv[1]
nb_bins = int(sys.argv[2])
max_load = int(sys.argv[3])
group = {}

with open(csv_file, "r") as csv:
    for line in csv.readlines():
        gid, gnumber = line.split(";")
        if int(gnumber) > 0:
            group[gid] = int(gnumber)

# Variables creation
x_names = []
numbers = []

counter = 0

for gid, gnumber in group.items():
    numbers.append(gnumber)
    x_names.append([])
    for i in range(nb_bins):
        x_names[counter].append("x_{}_{}".format(gid, i))
    counter += 1

nb_active = len(numbers)

# 1. Objective function
print("Minimize")
print("\tobj: y")

# 2. Constraints
print("Subject To")

# 2.a Assignments constraints
for i in range(nb_active):
    beg = "\t"
    for j in range(nb_bins):
        print("{}{}".format(beg, x_names[i][j]), end=" ")
        beg = "+ "
    print(" = 1")

# 2.b Load constraints
for j in range(nb_bins):
    beg = "\t"
    for i in range(nb_active):
        print("{}{} {}".format(beg, numbers[i], x_names[i][j]), end=" ")
        beg = "+"
    print(" <= {}".format(max_load))

# 2.c Link constraint
for j in range(nb_bins):
    beg = "\t"
    for i in range(nb_active):
        print("{}{} {}".format(beg, numbers[i], x_names[i][j]), end=" ")
        beg = "+"
    print("- y <= 0")

# 2.d Symmetry break
count = Counter(numbers)
for k, c in count.items():
    if c > 1:
        indices = [i for i, x in enumerate(numbers) if x == c]
        for i1 in range(len(indices)):
            for i2 in range(i1 + 1, len(indices)):
                for j1 in range(nb_bins):
                    beg = "\t"
                    for j2 in range(j1 + 1):
                        print("{}{} - {}".format(beg,
                                                 x_names[i1][j2],
                                                 x_names[i2][j2]), end=" ")
                        beg = "+"
                    print(" >= 0")

# 3. Variables domains
print("Binary")
for i in range(nb_active):
    for j in range(nb_bins):
        print("\t{}".format(x_names[i][j]))

print("Integer")
print("\ty")
print("End")
